/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolgenealogico;

import java.util.Scanner;

/**
 *
 * @author 1DAM
 */
public class Persona {
 
    
    //Añadir a cada persona un padre y una madre
    //añadir a cada persona una pareja
    //añadir a cada persona un array de hijos
    private String nombre;             ////////////////PRIVATE HACE QUE SOLO SE PUEDA ACCEDER AL VALOR DESDE LA CLASE
    private String primerApellido;
    private String segundoApellido;
    private boolean sexo; ///true femenino,false masculino
    private int edad;
    private Persona madre;
    private Persona padre;
    private Persona pareja;
    private Persona[]hijos;
    
public Persona(String nombre,String primerApellido,String segundoApellido,boolean sexo,int edad){
    
    if(Persona.esNombreAceptable(nombre)){          ///////COMPRUEBA EL NOMBRE USANDO LA FUNCION QUE HEMOS CREADO MAS ABAJO
        this.nombre=nombre;
    }else{
        this.nombre="NO";
    }
    if(edad>=0&&edad<120){
        this.edad=edad;
    }else{
        this.edad=0;
    }
    this.primerApellido=primerApellido;
    this.segundoApellido=segundoApellido;
    this.sexo=sexo;
    
}


public void asignaParentescos(Persona madre,Persona padre,Persona pareja,Persona[]hijos){
    this.madre=madre;
    this.padre=padre;
    this.pareja=pareja;
    this.hijos=hijos;
}

public Persona getMadre(){
       return this.madre;
   //  "La madre de "+this.nombre+" "+this.primerApellido+" "+this.segundoApellido+" es "+this.madre.nombre;
}
public Persona getPareja(){
    return this.pareja;
   // return "La pareja de "+this.nombre+" "+this.primerApellido+" "+this.segundoApellido+" es "+this.pareja;
}
public Persona getPadre(){
    //return "El padre  de "+this.nombre+" "+this.primerApellido+" "+this.segundoApellido+" es "+this.padre;
    return this.padre;
}

public Persona[] getHijos(){
    return hijos;
}

public int getNHijos(){
    return hijos.length;
}
/**
 * Devuelve el hijo nº indice que tuvo esa persona.Es importante que nunca se ponga un indice mayor que el numero de hijos
 * @param indice
 * @return 
 */
public Persona getHijo(int indice){
    if(indice>hijos.length){
        return null;
    }
       return this.hijos[indice];  
    
}
public static boolean esNombreAceptable(String nombre){
    if(nombre.equalsIgnoreCase("mamon")){
        return false;
    }
        return true;
}   


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////LOS GETTER NO TIENEN ARGUMENTOS A NO SE QUE SEA PARA COGER UN ELEMENTO DE UN ARRAY                 /////////
///////DEVUELVEN EL MISMO TIPO QUE SEA LA VARIABLE INTERNA.COMO NOMBRE ES STRING                          /////////
///////NORMALMENTE LO UNICO QUE HACE ES DEVOLVER EL VALOR DE LA VARIABLE, PERO PODIAMOS HACER OTRAS COSAS /////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public String getNombre(){
    return this.nombre;
}
public String getPrimerApellido(){
    return this.primerApellido;
}
public String getSegundoApellido(){
    return this.segundoApellido;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////LOS SETTER DEVUELVEN VOID
//////RECIBE POR PARAMETRO UN OBJETO DEL MISMO TIPO QUE LA VARIABLE QUE QUERAMOS ESTABLECER EL VALOR, A NO SE QUE QUERAMOS COMPROBACIONES DE SEGURIDAD///
///////////////////////////////////////////////////////////////////////////////////////////
public void setNombre(String nombre){
    if(Persona.esNombreAceptable(nombre)){          ///////COMPRUEBA EL NOMBRE USANDO LA FUNCION QUE HEMOS CREADO MAS ABAJO
        this.nombre=nombre;
    }else{
        this.nombre="NO";
    }
}

public String getSexo(){
    if(this.sexo==false){
        return "Hombre";
    }else{
        return "Mujer";
    }   
}
/**
 * Establece el sexo de la persona.Espera recibir hombre o mujer, si no asigna mujer
 * @param nuevoS El string que contiene el nuevo sexo, que deberia ser hombre o mujer
 */
public void setSexo(String nuevoS){
    if(nuevoS.equalsIgnoreCase("hombre")){
        this.sexo=false;
    }
    this.sexo=true;
}

public void setHijos(Persona[]h){
    hijos=h;
}

public boolean registrocivilCambiarNombre(Persona p){
    Scanner sc=new Scanner(System.in);
    System.out.println("Dime el nuevo nombre que qyuieres poner");
    String nuevoNombre=sc.nextLine();
    if(Persona.esNombreAceptable(nuevoNombre)==true){
        p.setNombre(nuevoNombre);
    }else{
        System.out.println("No puedes ponerte ese nombre");
        return false;
    }
    return true;
    
}
public void añadirHijo(Persona p, int pos){
    
}

}


