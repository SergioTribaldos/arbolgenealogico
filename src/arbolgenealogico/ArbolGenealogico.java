/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolgenealogico;

import java.util.Scanner;

/**
 *
 * @author 1DAM
 */
public class ArbolGenealogico {

     ///Con esta clase en un main, crear 3 generaciones de personas
    //
    //Abuela3 Abuelo3 Abuela2 abuelo2 Abuela1 Abuelo1
    //  |       |       |        |      |       |
    //  --------        ---------       ---------
    //      |             |    |            |
    //    madre2       padre2 madre1       padre1
    //      |             |     |            |
    //      --------------      --------------
    //       |          |             |
    //      hija2     hijo2         hijo1
    //construir este arbol genealogico usando la clase persona
    //hacer sout (hijo1.getMadre.getPareja().getPadre())
    public static void main(String[] args) {
        
        /////////INICIALIZAMOS DATOS BASICOS/////////
        Persona abuela3=new Persona("Jacinta","Stalin","Ramirez",true,88);
        Persona abuelo3=new Persona("Jacinto","Zapata","Grinder",false,78);
        Persona abuela2=new Persona("Puri","Hitler","Ramirez",true,67);
        Persona abuelo2=new Persona("Onofre","Ortega","Jander",false,76);
        Persona abuela1=new Persona("Bulma","yGasset","Bueno",true,65);
        Persona abuelo1=new Persona("Goku","Guevara","Grinder",false,94);
        
        Persona madre2=new Persona("Chun-Li",abuelo3.getNombre(),abuela3.getPrimerApellido(),true,44);
        Persona padre2=new Persona("Zangief",abuelo2.getPrimerApellido(),abuela2.getPrimerApellido(),false,43);
        Persona madre1=new Persona("Aerith",abuelo2.getPrimerApellido(),abuela2.getPrimerApellido(),true,51);
        Persona padre1=new Persona("Sergio",abuelo1.getPrimerApellido(),abuela1.getPrimerApellido(),false,39);
        
        Persona hija2=new Persona("Teresa",padre2.getPrimerApellido(),madre2.getPrimerApellido(),true,12);
        Persona hijo2=new Persona("Adolf",padre2.getPrimerApellido(),madre2.getPrimerApellido(),true,8);
        Persona hijo1=new Persona("Mahatma",padre1.getPrimerApellido(),madre1.getPrimerApellido(),true,1);
        
        ////////METEMOS A LOS HIJOS EN EL ARRAY//////
        Persona[]abuela3Hijos={madre2};
        Persona[]abuela2Hijos={padre2,madre1};
        Persona[]abuela1Hijos={padre1};
 
        Persona[]madre2Hijos={hija2,hijo2};     
        Persona[]madre1Hijos={hijo1};
     
        
        
        //////ASIGNAMOS LOS PARENTESCOS (DESPUES DE INTRODUCIR LOS DATOS BASICOS DE CADA UNO)///////////
        abuela3.asignaParentescos(null,null,abuelo3,abuela3Hijos);
        abuelo3.asignaParentescos(null,null,abuela3,abuela3Hijos);
        abuela2.asignaParentescos(null,null,abuelo2,abuela2Hijos);
        abuelo2.asignaParentescos(null,null,abuela2,abuela2Hijos);
        abuela1.asignaParentescos(null,null,abuelo1,abuela1Hijos);
        abuelo1.asignaParentescos(null,null,abuela1,abuela1Hijos);
        
        madre2.asignaParentescos(abuela3,abuelo3,padre2,madre2Hijos);
        padre2.asignaParentescos(abuela2,abuelo2,madre2,madre2Hijos);
        madre1.asignaParentescos(abuela2,abuelo2,padre1,madre1Hijos);
        padre1.asignaParentescos(abuela1,abuelo1,madre1,madre1Hijos);
        
        hija2.asignaParentescos(madre2,padre2,null,null);
        hijo2.asignaParentescos(madre2,padre2,null,null);
        hijo1.asignaParentescos(madre1,padre1,null,null);
        
        System.out.println("Los abuelos de "+hijo1.getNombre()+" "+hijo1.getPrimerApellido()+" son "+hijo1.getMadre().getPareja().getPadre().getNombre()+" "
                    +hijo1.getMadre().getPareja().getPadre().getPrimerApellido()+" y "+
                    hijo1.getMadre().getPadre().getNombre()+" "+hijo1.getMadre().getPadre().getPrimerApellido() ); 
        
        
        System.out.println(hijo1.getMadre().getPareja().getPadre().getNombre());
        System.out.println(abuelo2.getNombre()+abuelo2.getPrimerApellido()+abuelo2.getSegundoApellido());
        
       
        
    }
    
    public boolean cambiarNombre(Persona p){
        Scanner sc=new Scanner(System.in);
        System.out.println("Dime el nombre que quieres poner");
        String nuevoNombre=sc.nextLine();
        if(Persona.esNombreAceptable(nuevoNombre)){
            return true;
        }else{
            System.out.println("No te puedes poner ese nombre");
            return false;
        }
    }
    
}
